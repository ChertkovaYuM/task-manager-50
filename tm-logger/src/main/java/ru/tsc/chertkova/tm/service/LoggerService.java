package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.*;
import ru.tsc.chertkova.tm.api.service.ILoggerService;
import ru.tsc.chertkova.tm.model.EntityLog;

import java.io.File;
import java.io.FileOutputStream;

import static ru.tsc.chertkova.tm.constant.FileNameConst.*;

public class LoggerService implements ILoggerService {

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final EntityLog message) {
        @Nullable final String className = message.getClassName();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdirs();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        @NotNull final String header = "Id: " + message.getId() + "; Type: " + message.getType() + "; Date: " + message.getDate() + "\n";
        fileOutputStream.write(header.getBytes());
        fileOutputStream.write(message.getEntity().getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    private String getFileName(@NotNull final String className) {
        switch (className) {
            case "ProjectDTO":
            case "Project":
                return PROJECT_LOG_FILE_NAME;
            case "TaskDTO":
            case "Task":
                return TASK_LOG_FILE_NAME;
            case "UserDTO":
            case "User":
                return USER_LOG_FILE_NAME;
            case "SessionDTO":
            case "Session":
                return SESSION_LOG_FILE_NAME;
            default:
                return null;
        }
    }

}
