package ru.tsc.chertkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.project.ProjectCreateRequest;
import ru.tsc.chertkova.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        //System.out.println("ENTER DATE BEGIN:");
        //@Nullable final Date dateBegin = TerminalUtil.nextDate();
        //System.out.println("ENTER DATE END:");
        //@Nullable final Date dateEnd = TerminalUtil.nextDate();
        getServiceLocator().getProjectEndpoint().createProject(new ProjectCreateRequest(getToken(), name, description));
    }

}
