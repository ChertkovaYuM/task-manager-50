package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    Task unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

}
