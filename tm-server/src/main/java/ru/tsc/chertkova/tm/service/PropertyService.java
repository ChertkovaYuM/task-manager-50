package ru.tsc.chertkova.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "356585985";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    public static final String SERVER_HOST_KEY_DEFAULT = "localhost";

    @NotNull
    public static final String SESSION_KEY = "session.key";

    @NotNull
    public static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    public static final String DATABASE_USERNAME = "database.username";

    @NotNull
    public static final String DATABASE_PASSWORD = "database.password";

    @NotNull
    public static final String DATABASE_URL = "database.url";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_DIALECT = "database.dialect";

    @NotNull
    private static final String DATABASE_HBM2DDL = "database.hbm2ddl";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_USE_SECOND_LVL_CACHE_ENVIRONMENT_VARIABLE = "DATABASE_SECOND_LVL_CACHE";

    @NotNull
    private static final String DATABASE_USE_SECOND_LVL_CACHE_PROPERTY = "database.cache.use_second_level_cache";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_ENVIRONMENT_VARIABLE = "DATABASE_QUERY_CACHE";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_PROPERTY = "database.cache.use_query_cache";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS_ENVIRONMENT_VARIABLE = "DATABASE_CACHE_USE_MINIMAL_PUTS";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS_PROPERTY = "database.cache.use_minimal_puts";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX_ENVIRONMENT_VARIABLE = "DATABASE_CACHE_REGION_PREFIX";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX_PROPERTY = "database.cache.region_prefix";

    @NotNull
    private static final String DATABASE_CACHE_CONFIG_FILE_ENVIRONMENT_VARIABLE = "DATABASE_CACHE_CONFIG_FILE";

    @NotNull
    private static final String DATABASE_CACHE_CONFIG_FILE_PROPERTY = "database.cache.provider_configuration_file_resource_path";

    @NotNull
    private static final String DATABASE_CACHE_FACTORY_CLASS_ENVIRONMENT_VARIABLE = "DATABASE_CACHE_FACTORY_CLASS";

    @NotNull
    private static final String DATABASE_CACHE_FACTORY_CLASS_PROPERTY = "database.cache.region.factory_class";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key,
                                  @NotNull final String defaultValue) {
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getProperties().getProperty(envKey);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String value = getStringValue(SERVER_HOST_KEY, SERVER_HOST_KEY_DEFAULT);
        return value;
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return Integer.parseInt(getStringValue(SESSION_TIMEOUT));
    }

    @NotNull
    @Override
    public String getDatabaseUsername() {
        return getStringValue(DATABASE_USERNAME);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DATABASE_URL);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT);
    }

    @NotNull
    @Override
    public String getDatabaseHBM2DDL() {
        return getStringValue(DATABASE_HBM2DDL);
    }

    @NotNull
    @Override
    public String getDatabaseShowSQL() {
        return getStringValue(DATABASE_SHOW_SQL);
    }

    @NotNull
    @Override
    public String getDatabaseUseSecondLvlCache() {
        return getStringValue(DATABASE_USE_SECOND_LVL_CACHE_ENVIRONMENT_VARIABLE,
                DATABASE_USE_SECOND_LVL_CACHE_PROPERTY);
    }

    @NotNull
    @Override
    public String getDatabaseUseQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE_ENVIRONMENT_VARIABLE,
                DATABASE_USE_QUERY_CACHE_PROPERTY);
    }

    @NotNull
    @Override
    public String getDatabaseUseMinimalPuts() {
        return getStringValue(DATABASE_CACHE_USE_MINIMAL_PUTS_ENVIRONMENT_VARIABLE,
                DATABASE_CACHE_USE_MINIMAL_PUTS_PROPERTY);
    }

    @NotNull
    @Override
    public String getDatabaseCacheRegionPrefix() {
        return getStringValue(DATABASE_CACHE_REGION_PREFIX_ENVIRONMENT_VARIABLE,
                DATABASE_CACHE_REGION_PREFIX_PROPERTY);
    }

    @NotNull
    @Override
    public String getDatabaseCacheConfigFile() {
        return getStringValue(
                DATABASE_CACHE_CONFIG_FILE_ENVIRONMENT_VARIABLE,
                DATABASE_CACHE_CONFIG_FILE_PROPERTY);
    }

    @NotNull
    @Override
    public String getDatabaseCacheFactoryClass() {
        return getStringValue(DATABASE_CACHE_FACTORY_CLASS_ENVIRONMENT_VARIABLE,
                DATABASE_CACHE_FACTORY_CLASS_PROPERTY);
    }

}
