package ru.tsc.chertkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractUserOwnerModelRepository<Task> {

    void add(@NotNull String userId,
             @NotNull Task model);

    void clear(@NotNull String userId);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @Nullable
    Task findById(@NotNull String userId,
                  @NotNull String id);

    int getSize(@NotNull String userId);

    Task removeById(@NotNull String userId,
                    @NotNull String id);

    void update(@NotNull String userId,
                Task model);

    @Nullable
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId);

    void changeStatus(@NotNull String id,
                      @NotNull String userId,
                      @NotNull String status);

    int existsById(@Nullable String id);

    void bindTaskToProject(@NotNull String taskId,
                           @NotNull String projectId,
                           @NotNull String userId);

}
